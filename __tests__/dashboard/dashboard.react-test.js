'use strict';

/* globals expect */
import React from 'react';
import Dashboard from '../../client/app/dashboards/dashboard.jsx';
import renderer from 'react-test-renderer';

test('Dashboard test', () => {
    let component = renderer.create(
        <Dashboard type="line" handleButtonClick={() => {}} />
    );
    
    let tree = component.toJSON();
    
    expect(tree).toMatchSnapshot();
});