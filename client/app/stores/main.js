import {createStore} from 'redux';
import dashboardApp from '../reducers/dashboardApp.js';

export const STORE = createStore(dashboardApp);
