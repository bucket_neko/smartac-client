'use strict';

import React from 'react';
import {BarChart, XAxis, YAxis, Bar, Tooltip} from 'recharts';

class BarGraph extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <BarChart {...this.props}>
                <XAxis dataKey="name" />
                <YAxis dataKey="value" />
                <Bar dataKey="value" fill="#8884d8" />
                <Tooltip />
            </BarChart>
        );
    }
}

export default BarGraph;