'use strict';

import React from 'react';
import {PieChart, Pie} from 'recharts';

class PieGraph extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <PieChart {...this.props}>
                <Pie data={this.props.data} outerRadius="50%" fill="#8884d8" label />
            </PieChart>
        );
    }
}

export default PieGraph;