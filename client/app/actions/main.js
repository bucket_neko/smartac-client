/**
 * Needed actions:
 * get stored dashboard
 * create new dashboard
 * updated dashboard
 * delete dashbord
 * 
 * Basic CRIUD
 * 
 * 
 */
 
 
export const RETRIEVE_DASHBOARDS = "RETRIEVE_DASHBOARDS";
export const CREATE_NEW_DASHBOARD = "CREATE_NEW_DASHBOARD";
export const UPDATE_DASHBOARD = "UPDATE_DASHBOARD";
export const DELETE_DASHBOARD = "DELETE_DASHBOARD";
export const UPDATE_DATA = "UPDATE_DATA";

export const ACTIONS = {
    RETRIEVE_DASHBOARDS, CREATE_NEW_DASHBOARD,
    UPDATE_DASHBOARD, DELETE_DASHBOARD, UPDATE_DATA
};

export function retrieveDashboards(payload) {
 return {
     type: RETRIEVE_DASHBOARDS,
     payload
 };
}

export function createDashboard(payload) {
 return {
     type: CREATE_NEW_DASHBOARD,
     payload: Object.assign({}, payload)
 };
}

export function updateDashboard(index, payload) {
 return {
     type: UPDATE_DASHBOARD,
     index,
     payload: Object.assign({}, payload)
 };
}

export function deleteDashboard(index) {
 return {
     type: DELETE_DASHBOARD,
     index
 };
}

export function updateData(index) {
    let data = Array(3);
    let name = ['one','two','three'];
    
    for(let count = 0; count < data.length; count += 1){
        data[count] = {
                name: name[count],
                value: Math.floor(Math.random() * 20000)
        };
    }    
    
    return {
        type: UPDATE_DATA,
        index,
        payload: {data}
    };
}