import React from 'react';
import ReactDOM from 'react-dom';
import DashboardContainer from './dashboards/dashboard-container.jsx';


ReactDOM.render(
    <DashboardContainer />,
    document.getElementById('app')
);
