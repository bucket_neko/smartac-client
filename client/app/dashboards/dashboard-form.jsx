import React from 'react';

class DashboardForm extends React.Component {
    constructor(props) {
        super(props);
        this.widths = Array(6).fill(1)
            .map( (_, index) => {return {index, value: (index * 50) + 150}});
        this.heights = Array(3).fill(1)
            .map( (_, index) => {return {index, value: (index * 50) + 150}});
        this.types = ['Bar', 'Pie'];
        this.handleButtonClick = this.handleButtonClick.bind(this);
    }
    
    handleButtonClick(e) {
        this.props.handleButtonClick(e, this);
    }
    
    render() {
        return (
            <div className={this.props.className}>
                <label>Chart Width:
                    <select ref={ (select) => this.widthSelect = select } defaultValue={this.props.width}>
                        {
                            this.widths.map(
                                ({index, value}) => <option key={index}>{value}</option>
                            )
                        }
                    </select>
                </label>
                <label>Chart Height:
                    <select ref={ (select) => this.heightSelect = select } defaultValue={this.props.height}>
                        {
                            this.heights.map(
                                ({index, value}) => <option key={index}>{value}</option>
                            )
                        }
                    </select>
                </label>
                <label>Chart Type:
                    <select ref={ (select) => this.typeSelect = select } defaultValue={this.props.type}>
                        {
                            this.types.map(
                             (type,index) => <option key={index}>{type}</option>
                            )
                        }
                    </select>
                </label>
                <button onClick={this.handleButtonClick.bind(this)}>{this.props.buttonTitle || 'Add Dashboard'}</button>
            </div>
        );
    }
}

export default DashboardForm;