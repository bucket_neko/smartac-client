import React from 'react';
import BarGraph from '../charts/bar.jsx';
import PieGraph from '../charts/pie.jsx';
import DashboardForm from './dashboard-form.jsx';
import {STORE} from '../stores/main.js';
import {retrieveDashboards} from '../actions/main.js';
import {getData, updateDashboard, delDashboard} from '../services/services.js';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data:[]};
        
        this.handleRemove = this.handleRemove.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
    }
    
    handleRefresh() {
        let setState = this.setState.bind(this);
        getData()
        .then( data => setState({data}));
    }
    
    handleUpdate(e, ref) {
        let {heightSelect, widthSelect, typeSelect} = ref;
        updateDashboard(
            this.props['data-index'],
            {
                height: Number(heightSelect.value),
                width: Number(widthSelect.value),
                type: typeSelect.value
            }
        )
        .then( dashboards => STORE.dispatch(retrieveDashboards({dashboards})));
    }
    
    handleRemove(e) {
        delDashboard(this.props['data-index'])
        .then( dashboards => STORE.dispatch(retrieveDashboards({dashboards})));
    }
    
    componentDidMount() {
        this.handleRefresh();
    }
    
    render() {
        let chart = '';
        
        switch(this.props.type.toLowerCase()) {
            case 'pie':
                chart = (<PieGraph {...this.props} data={this.state.data} />)
                break;
            default:
                chart = (<BarGraph {...this.props} data={this.state.data}/>);
        }
        
        return (
        <div className="dashboard">
            <div className="dashboard-chart">
                <div className="dashboard-content">{chart}</div>
                <div className="dashboard-buttons">
                    <button onClick={this.handleRefresh}>Refresh</button>
                    <button onClick={this.handleRemove}>Remove</button>
                </div>
            </div>
            <DashboardForm {...this.props} buttonTitle="Update" className="dashboard-update-section" handleButtonClick={this.handleUpdate} />
        </div>
        );
    }
}

export default Dashboard;