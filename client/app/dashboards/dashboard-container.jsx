/* global fetch */
'use strict';
import React from 'react';
import Dashboard from './dashboard.jsx';
import DashboardForm from './dashboard-form.jsx';
import {STORE} from '../stores/main.js';
import {createDashboard, retrieveDashboards} from '../actions/main.js';
import {getDashboards, addDashboard} from '../services/services.js';

class DashboardContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dashboards: []
        };

        this.handleAddDashboard = this.handleAddDashboard.bind(this);
    }
    
    handleAddDashboard(e, refs) {
        let setState = this.setState.bind(this);
        
        addDashboard({
            height: Number(refs.heightSelect.value),
            width: Number(refs.widthSelect.value),
            type: refs.typeSelect.value,
        })
        .then( dashboards => setState({dashboards}));

    }
    
    componentDidMount() {
        let setState = this.setState.bind(this);
        this.unsubscribeFromStore = STORE.subscribe( () => {
            let newState = STORE.getState();
            setState(newState);
        });
        getDashboards()
        .then( dashboards => 
            setState({dashboards})
        );
    }
    
    componentWillMount() {
        if (typeof(this.unsubscribeFromStore) === 'function'){
            this.unsubscribeFromStore();
        }
    }
    
    render(){
        return (
        <div className="dashboard-container">
            <DashboardForm handleButtonClick={this.handleAddDashboard} />
            {
                this.state.dashboards.map( (dashboard, index) => <Dashboard key={index} data-index={index} {...dashboard} /> )
            }
        </div>);
    }
}

export default DashboardContainer;