/* globals fetch */
'use strict';

import 'whatwg-fetch';

const URL = {
    dashboard: 'https://smartrac-server-jsneko.c9users.io:8080/dashboard',
    data: 'https://smartrac-server-jsneko.c9users.io:8080/data'
};

const headers = {
    'Content-Type': 'application/json'
};

export function getDashboards() {
    
    return fetch(URL.dashboard)
    .then( response => response.json());
    
}

export function addDashboard(body) {
    return fetch(URL.dashboard,{
        method: 'POST',
        headers,
        body: JSON.stringify(body)
    })
    .then( response => response.json());
}

export function updateDashboard(id, body) {
    return fetch(`${URL.dashboard}/${id}`,{
        method: 'PUT',
        headers,
        body: JSON.stringify(body)
    })
    .then( response => response.json());
}

export function delDashboard(id) {
    return fetch(`${URL.dashboard}/${id}`,{
        method: 'DELETE',
        headers
    })
    .then( response => response.json());
}

export function getData() {
    return fetch(URL.data)
    .then( response => response.json());
}