import {ACTIONS} from '../actions/main.js';

const initial = {
    dashboards: []
};

function dashboardApp(state = initial, action) {
    let dashboards = [].concat(state.dashboards);
    let current;
    switch(action.type) {
        case ACTIONS.CREATE_NEW_DASHBOARD:
            dashboards.push(action.payload);
            return Object.assign({}, state, {dashboards});
        case ACTIONS.UPDATE_DASHBOARD:
            if (action.index < 0 || action.index > dashboards.length - 1) {
                return state;
            }
            current = dashboards[action.index];
            dashboards[action.index] = Object.assign({},current,action.payload);
            return Object.assign({},state,{dashboards});
        case ACTIONS.DELETE_DASHBOARD:
            if (action.index < 0 || action.index > dashboards.length - 1) {
                return state;
            }
            dashboards.splice(action.index,1);
            return Object.assign({}, state, {dashboards});
        case ACTIONS.UPDATE_DATA:
            if (action.index < 0 || action.index > dashboards.length - 1) {
                return state;
            }
            current = dashboards[action.index];
            dashboards[action.index] = Object.assign({},current,action.payload);
            return Object.assign({},state,dashboards);
        default:
            return Object.assign({},state,action.payload);
    }
}

export default dashboardApp;